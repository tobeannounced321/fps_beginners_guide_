﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_Controller : MonoBehaviour
{

    public GameObject NPCTextPrefab;
    public GameObject playerObject;
    public string dialogueString;
    public float leaveTimer = 5f;
    public Camera playerCamera;

    bool timerStart = false;


    // Start is called before the first frame update
    void Start()
    {
        PlayerCharacterController playerController = playerObject.GetComponent<PlayerCharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(timerStart == true)
        {
            leaveTimer -= Time.deltaTime;
        }

        if(leaveTimer <= 0)
        {
            NPCLeave();
        }

        //GetComponentInChildren<TextMesh>().transform.LookAt(playerCamera.transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (NPCTextPrefab)
            {
                ShowNPCText();
            }

            if (playerObject.GetComponent<PlayerCharacterController>().TextCheck != 1)
            {
                playerObject.GetComponent<PlayerCharacterController>().TextCheck = 1;
            }

            if (playerObject.GetComponent<PlayerCharacterController>().TextCheck == 1)
            {
                GetComponentInChildren<TextMesh>().text = dialogueString;
                timerStart = true;
            }
            print(playerObject.GetComponent<PlayerCharacterController>().TextCheck);
            //print("AH A Player!");
        }
    }

    void ShowNPCText()
    {
        Instantiate(NPCTextPrefab, transform.position, Quaternion.identity, transform);
    }

    void NPCLeave()
    {
        Destroy(transform.parent.gameObject);
    }

}
