﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour
{
    public string sceneName = "";
    public float startTimer = 5f;
    public bool mainLoaded = false;

    void Update()
    {
        startTimer -= Time.deltaTime;
        print(startTimer);
        if (startTimer <= 0 && mainLoaded == false)
        {
            LoadTargetScene();
            mainLoaded = true;
        }
    }

    public void LoadTargetScene()
    {
        SceneManager.LoadScene(sceneName);
    }
}
