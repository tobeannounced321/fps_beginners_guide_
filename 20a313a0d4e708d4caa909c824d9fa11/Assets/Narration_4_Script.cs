﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Narration_4_Script : MonoBehaviour
{

    public AudioSource playerAudio;
    public AudioClip fourthNarration;
    public bool narrate = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && narrate == false)
        {
            playerAudio.PlayOneShot(fourthNarration);
            narrate = true;
        }
    }
}
