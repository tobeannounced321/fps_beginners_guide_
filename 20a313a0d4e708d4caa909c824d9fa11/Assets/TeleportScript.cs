﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportScript : MonoBehaviour
{

    public float teleportTimer;
    public GameObject playerObject;
    public Transform teleportPoint;
    public bool teleportReady = false;
    public AudioSource playerAudio;
    public AudioClip thirdNarration;
    public bool narrate = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(teleportReady == true)
        {
            teleportTimer -= Time.deltaTime;
            print(teleportTimer);
        }

        if(teleportTimer <= 0f && teleportReady == true)
        {
            playerObject.transform.position = teleportPoint.transform.position;
            teleportReady = false;
            print("teleport");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && narrate == false)
        {
            teleportReady = true;
            playerAudio.PlayOneShot(thirdNarration);
            narrate = true;
        }
    }
}
